$(function() {
  setTimeout(() => {  // delay by 3 secs.
    let offer = $(".toast")
    offer.toast({delay: 6000})
    offer.on("hidden.bs.toast", () => {
      offer.toast("dispose")
    })
    offer.toast("show")
  },3000)
})
