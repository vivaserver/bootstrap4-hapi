const Hapi = require("hapi");
const Inert = require("inert")
const Vision = require("vision")
const Pug = require("pug");

const server = async function() {
  const app = new Hapi.Server({port: process.env.PORT || 5000})
  try {
    await app.register(Inert)
    await app.register(Vision)
    app.views({
      engines: { pug: Pug },
      relativeTo: __dirname,
      path: "app/templates",
      compileOptions: { pretty: true }
    })
    app.route(require("./app/controllers/assets"))
    app.route(require("./app/controllers/home"));
    await app.start()
    console.log("server@%s env:%s",app.info.uri,process.env["NODE_ENV"] || "?")
  }
  catch (err) {
    console.error(err)
  }
}

server()

// vim: nofoldenable
