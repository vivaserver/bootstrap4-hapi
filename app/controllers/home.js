module.exports = []
  .concat({
    path: "/",
    method: "GET",
    handler: (request, h) => {
      return h.view("home")
    }
  })
  .concat({
    path: "/p/{page}",
    method: "GET",
    handler: (request, h) => {
      return h.view(request.params.page)
    }
  })
